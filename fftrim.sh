#!/bin/bash

# fftrim.sh
# helping you trimming a video
# 
# https://codeberg.org/jrm-omg/fftools

# install, so you can run it from everywhere
# sudo cp fftrim.sh /usr/local/bin/

if [ -z $1 ]; then
  echo ""
  echo "Usage: fftrim.sh <input> <start> <end>"
  echo "Demo : fftrim.sh job.mp4 00:02:31 02:45:38"
  exit
fi

# input modifier (if using find)
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# input pathinfo
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}")
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

ffmpeg -ss $2 -to $3 -i "$input" -c copy "$input_filename-e.$input_extension"
