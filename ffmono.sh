#!/bin/bash

# ffmono.sh
# helping you mapping audio from a video
# 
# https://codeberg.org/jrm-omg/fftools

# install, so you can run it from everywhere
# sudo cp ffmono.sh /usr/local/bin/

if [ -z $1 ]; then
  echo ""
  echo "Usage: ffmono.sh <input>"
  echo "Demo : ffmono.sh video-with-only-one-audio-channel.mp4"
  echo ""
  echo "Batch : find -maxdepth 1 -iname \"*.mp4\" -exec ffmono.sh {} \;"
  exit
fi

# input modifier (if using find)
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# input pathinfo
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}")
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

# c0=c0 # use only channel 0
# c0=c1 # use only channel 1
# https://trac.ffmpeg.org/wiki/AudioChannelManipulation
ffmpeg -i "$input" -vcodec copy -af "pan=mono|c0=c0" "$input_filename-m.$input_extension"
