# x264-Expecto-Patronum

"x264-Expecto-Patronum" is a dump bash script that might help you compressing high quality x264 videos, at a small size, which is cool for the web, doesn't it ?

This script comes from many [readings](https://www.lighterra.com/papers/videoencodingh264/), reverse engineering (from the outstanding YIFY's releases), hundreds of tests, mad silly comparaisons... And I hope you will enjoy using it `<3`

```sh
╔══════════════════════════════════════════╗
║ ▓▒░         Expecto Patronum         ░▒▓ ║
╚══════════════════════════════════════════╝

      (             )
       `--(_   _)--'
            Y-Y
           /@@ \
          /     \
          `--'.  \\             ,
              \|   `.__________/)

╔══════════════════════════════════════════╗
║ ▓▒░         Expecto Patronum         ░▒▓ ║
╚══════════════════════════════════════════╝
```

## Requirements 

Grab the [`x264-Expecto-Patronum.sh`](x264-Expecto-Patronum.sh) script and make sure you have `ffmpeg` installed with the `--enable-libfdk-aac` flag. If you really want to have the latest ffmpeg you can also [compile it yourself](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu) (more platforms [here](https://trac.ffmpeg.org/wiki/CompilationGuide)).

## Installation

```sh
sudo wget https://codeberg.org/jrm-omg/fftools/raw/branch/main/x264-Expecto-Patronum.sh -O /usr/local/bin/x264-Expecto-Patronum.sh; \
sudo chmod +x /usr/local/bin/x264-Expecto-Patronum.sh;
```

## How to use

The easiest way to test the script is : `x264-Expecto-Patronum.sh <input>` this will compress your `<input file>` to 480p, which is perfect for mobiles and/or average bandwidth.

## Presets

The preset option is the second argument that you may use :

```sh
x264-Expecto-Patronum.sh <input> <preset>
```

|  preset | video (x264) | audio (aac) | description |
|--------:|:------------:|:-----------:|-------------|
|    360p |     450k     |     64k     | [web] suitable for old school smartphones (iPhone 3GS) and/or low bandwidth (slow 3G) |
|    480p |     1000k    |     96k     | [web] default, perfect for all smartphones and/or average bandwidth (good 3G) |
|    720p |     2000k    |     128k    | [web] welcome to HD, perfect for all tablets, desktop and/or good bandwidth (4G) |
|   1080p |     3500k    |     128k    | [web] this is ultra HD, perfect for all desktop, TV and/or fast bandwidth |
| 1080pHQ |     7500k    |     160k    | [archive] this is more for archiving purposes |
|  crf23  |   -crf 23    |     160k    | [archive] if you want to compress a video for archiving purposes, keeping lots of details and without resizing it. Well that preset is for you. And it runs a little bit faster as the Constant Rate Factor (CRF) mode needs only one pass |
|  dvd    |   1500    |     128k    | [archive] if you want to compress a video that has been ripped from a DVD source |

For example, if I want to compress a `DJI_0081.MP4` video using the `720p` preset, I would go for :

```sh
x264-Expecto-Patronum.sh DJI_0081.MP4 720p
```

## Don't use this script..

..If you are hurry, if you just need to compress some videos as fast as possible. Because x264-Expecto-Patronum is using quite a lot of little tunings that gives one of the best size/quality ratio, but this has a cost, and this cost is the CPU encoding time !

If you don't need those CPU tunings parameters, just use [Constant Rate Factor](https://trac.ffmpeg.org/wiki/Encode/H.264#crf) (CRF)

- CRF 33 : regarding my [tests](https://codeberg.org/jrm-omg/fftools/src/branch/main/ff-x264-crf-tests.sh), CRF 33 gives a really good encoding time vs quality vs size ratio.
- CRF 38 : still in the game, less details than CRF 33 but no ugly blocks like CRF 40+, and almost half the size of CRF 33.

Single mode :

```sh
ffmpeg -y -i <name_of_your_input_file> -vf "scale=-2:1080" -c:v libx264 -crf 33 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart <name_of_your_output_file> \;
```

Batch mode :

```sh
find -maxdepth 1 -iname "*.mp4" -exec ffmpeg -y -i "{}" -vf "scale=-2:1080" -c:v libx264 -crf 33 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart "{}_CRF33.mp4" \;
```

## That's all folks

You may find even more options (like `audiocopy`, or `maxheight`) displaying the help screen, just type :

```sh
x264-Expecto-Patronum.sh
```

And you will see that help screen !

That's it ! Enjoy !

Made with `<3` by [Jérémie](https://humanize.me/now/)
