#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Built-in modules
import sys
import os
import time
import math

# External modules
try:
    import ffmpy
except ImportError as e:
    print('Error -> ', e)
    print('Please "pip install" all required packages first.')
    sys.exit()

# Splash
print('╔════════════════════════════════════════════════════════╗')
print('║ ff-crf-tests.py - https://codeberg.org/jrm-omg/fftools ║')
print('╚════════════════════════════════════════════════════════╝')
print('')

# Checkpoints
if len(sys.argv) < 5:
    print('Please provide all required arguments')
    print('Usage : ff-crf-tests.py <input> <vertical resolution> <encoder> <crf min> <crf max>')
    print('For instance : ff-crf-tests.py sample8k.mp4 480 libx264 20 35')
    print('\nDepending of your FFmpeg, encoders could be libx264, libx265, libsvtav1, etc.')
    print('Check the FFmpeg docs : https://ffmpeg.org/ffmpeg-codecs.html#Encoders')
    sys.exit()
if os.path.exists(sys.argv[1]) == False:
    print('[error] Input file does not exists.')
    sys.exit()

# Init
input_fn = sys.argv[1]
input_noext = os.path.splitext(os.path.basename(input_fn))[0]
output_height, encoder, crf_min, crf_max = sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]
output_folder = 'ff-crf-tests'
if os.path.isdir(output_folder) == False:
        os.mkdir(output_folder)

# Process
for crf in range(int(crf_min), int(crf_max)+1):
    output_fn = f'{input_noext}.{output_height}p.{encoder}.q{crf}.mp4'
    output_fp = output_folder + '/' + output_fn
    if os.path.isfile(output_fp) != True:
        ff = ffmpy.FFmpeg(
            inputs={input_fn: '-y -hide_banner -loglevel quiet -stats'},
            outputs={output_fp: f'-vf "scale=-2:{output_height}" -c:v {encoder} -crf {crf} -colorspace bt709 -movflags faststart -an'}
        )
        time_start = time.time_ns()
        ff.run()
        time_diff = math.floor((time.time_ns() - time_start) / 1000000000)
        print(f'[✓] {output_fn} encoded in {time_diff} seconds.')
    else:
        print(f'[-] skipping {output_fn} - already exists.')

# EOF
print('---')
print('Benchmark completed')
print(f'Encoded files have been saved to {output_folder}/')
print('See ya!')