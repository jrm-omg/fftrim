#!/bin/bash

# usage
# x264-Expecto-Patronum.sh <input> (<preset>)

# install
# sudo cp x264-Expecto-Patronum.sh /usr/local/bin/x264-Expecto-Patronum.sh; sudo chmod 555 /usr/local/bin/x264-Expecto-Patronum.sh

echo "╔══════════════════════════════════════════╗"
echo "║ ▓▒░         Expecto Patronum         ░▒▓ ║"
echo "╚══════════════════════════════════════════╝"
echo ""
echo "      (             )"
echo "       \`--(_   _)--'"
echo "            Y-Y"
echo "           /@@ \\"
echo "          /     \\"
echo "          \`--'.  \\\\             ,"
echo "              \\|   \`.__________/)"
echo ""
echo "╔══════════════════════════════════════════╗"
echo "║ ▓▒░         Expecto Patronum         ░▒▓ ║"
echo "╚══════════════════════════════════════════╝"
echo ""
echo "https://codeberg.org/jrm-omg/x264-Expecto-Patronum"
echo ""

# ------------------------------------------------
# fancy logo & colors & style
# ------------------------------------------------
greenBold="\e[1;32m"
reset="\e[0m"
whiteBold="\e[1;37m"
logo="${greenBold}[${whiteBold}xEp${greenBold}]${reset}"
underline="\e[4m"

# ------------------------------------------------
# check required
# ------------------------------------------------
if [ -z $1 ]; then
  echo -e "$logo x264-Expecto-Patronum.sh <input> (<preset>) (<audiocopy>) (<maxheight>)"
  echo -e "$logo --"
  echo -e "$logo <preset>    : 360p, 480p (default), 720p, 1080p, 1080pHQ, crf23, dvd"
  echo -e "$logo <audiocopy> : audiocopy, noaudio, false (default, audio will be reencoded)"
  echo -e "$logo <maxheight> : noresize or any height (in pixels)"
  echo -e "$logo --"
  echo -e "$logo Batch :"
  echo -e "$logo find -iname \"*.mp4\" -exec x264-Expecto-Patronum.sh {} 1080p audiocopy \\;"
  echo ""
  echo "Don't use this script if you hurry !"
  echo "in this case, simply do something like :"
  echo "find -maxdepth 1 -iname \"*.mp4\" -exec ffmpeg -y -i \"{}\" -vf \"scale=-2:720\" -c:v libx264 -crf 33 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart \"{}_CRF33.mp4\" \;"
  echo ""
  exit
fi

# ------------------------------------------------
# using linux "find" command ? remove "./" prefix
# ------------------------------------------------
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  echo -e "$logo looks like you're using find"
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# ------------------------------------------------
# input
# ------------------------------------------------
input_without_extension="${input%%.*}"

# ------------------------------------------------
# selected video preset
# ------------------------------------------------
if [ ! -z $2 ]; then
  preset=$2
else
  preset="480p" # default
fi
echo -e "$logo video preset : $preset"

# ------------------------------------------------
# selected audio preset
# ------------------------------------------------
if [ ! -z $3 ]; then
  audiocopy=$3
else
  audiocopy="false" # default
fi

# ------------------------------------------------
# video max height
# ------------------------------------------------
if [ ! -z $4 ]; then
  # forced max height
  outputMaxheight=$4
else
  # preset's max height :
  if [ $preset = crf23 ] || [ $preset = dvd ]; then
    outputMaxheight="noresize"
  elif [ $preset = 360p ]; then
    outputMaxheight=360
  elif [ $preset = 480p ]; then
    outputMaxheight=480
  elif [ $preset = 720p ]; then
    outputMaxheight=720
  elif [ $preset = 1080p ] || [ $preset = 1080pHQ ] || [ $preset = 1080pYIFY ]; then
    outputMaxheight=1080
  fi
fi

# ------------------------------------------------
# video will be resized, or not ?
# ------------------------------------------------
if [ $outputMaxheight = "noresize" ]; then
  filter=""
  echo -e "$logo output will not be resized"
else
  inputHeight=$(mediainfo --Inform='Video;%Height%' "$input")
  if [ $inputHeight -le $outputMaxheight ]; then
    filter=""
    echo -e "$logo input is smaller (or equal), output will not be resized"
  else
    filter=\-vf\ "scale=-2:$outputMaxheight"
    echo -e "$logo input is bigger, output will be resized to ${outputMaxheight}px"
  fi
fi

# ------------------------------------------------
# video preset
# ------------------------------------------------
if [ $preset = 360p ]; then
  video_params"-c:v libx264 -b:v 450k -maxrate 900k -bufsize 256k -profile:v baseline -level 3.0 -partitions +parti4x4+parti8x8+partp4x4+partp8x8+partb8x8 -colorspace bt709 -movflags faststart"
fi
if [ $preset = 480p ]; then
  bv=1000
fi
if [ $preset = 720p ]; then
  bv=2000
fi
if [ $preset = 1080p ]; then
  bv=3500
fi
if [ $preset = 1080pHQ ]; then
  bv=7500
fi
if [ $preset = dvd ]; then
  bv=1200
  mx=31250
  bf=31250
  video_params="-c:v libx264 -b:v ${bv}k -maxrate ${mx}k -bufsize ${bf}k -tune film -profile:v high -level 4.1 -me_method umh -me_range 24 -rc-lookahead 60 -subq 9 -refs 4 -b_strategy 2 -trellis 2 -psy-rd 1.00:0.15 -direct-pred 3 -keyint_min 23 -x264-params no-fast-pskip=1 -movflags faststart"
  # ↑ no colorspace here
fi
if [ $preset = 480p ] || [ $preset = 720p ] || [ $preset = 1080p ] || [ $preset = 1080pHQ ] || [ $preset = 1080pYIFY ]; then
  mx=$(echo "$bv * 2" | bc)
  bf=$(echo "$mx * 1.5" | bc)
  if [ $preset = 1080pYIFY ]; then
    bv=2250
    mx=31250
    bf=31250
  fi
  video_params="-c:v libx264 -b:v ${bv}k -maxrate ${mx}k -bufsize ${bf}k -tune film -profile:v high -level 4.1 -me_method umh -me_range 24 -rc-lookahead 60 -subq 9 -refs 4 -b_strategy 2 -trellis 2 -psy-rd 1.00:0.15 -direct-pred 3 -keyint_min 23 -x264-params no-fast-pskip=1 -colorspace bt709 -movflags faststart"
fi
if [ $preset = crf23 ]; then
  video_params="  -c:v libx264 -crf 23 -tune film -colorspace bt709 -movflags faststart"
fi
# ------------------------------------------------
# audiocopy
# ------------------------------------------------
if [ $audiocopy = "audiocopy" ]; then
  audio_params="-c:a copy"
  audio_tag="ac"
elif [ $audiocopy = "noaudio" ]; then
  audio_params="-an"
  audio_tag="noaudio"
else
  if [ $preset = 360p ]; then
    audio_params="-c:a libfdk_aac -b:a 64k -ac 1 " # mono
    audio_tag="aac.64k"
  fi
  if [ $preset = 480p ]; then
    audio_params="-c:a libfdk_aac -b:a 96k"
    audio_tag="aac.96k"
  fi
  if [ $preset = 720p ] || [ $preset = 1080p ] || [ $preset = 1080pYIFY ] || [ $preset = dvd ]; then
    audio_params="-c:a libfdk_aac -b:a 128k"
    audio_tag="aac.128k"
  fi
  if [ $preset = 1080pHQ ]; then
    audio_params="-c:a libfdk_aac -b:a 160k"
    audio_tag="aac.160k"
  fi
fi
echo -e "$logo audio preset : $audio_tag"

# ------------------------------------------------
# encode
# ------------------------------------------------
echo ""
outputTmp="$input_without_extension.$preset.x264.$audio_tag-ENCODING.mp4"
start=$SECONDS
if [ $preset = crf23 ]; then
  echo -e "$logo running a single pass"
  echo -e "$logo input : ${input}"
  echo -e "$logo output : ${outputTmp}"
  echo ""
  ffmpeg -y -i "$input" $filter $video_params $audio_params "$outputTmp"
else
  echo -e "$logo running first pass"
  echo -e "$logo input : ${input}"
  echo -e "$logo output : dev/null"
  echo ""
  ffmpeg -y -i "$input" $filter -pass 1 $video_params $audio_params -an -f mp4 /dev/null
  echo ""
  echo -e "$logo running second pass"
  echo -e "$logo input : ${input}"
  echo -e "$logo output : ${outputTmp}"
  echo ""
  ffmpeg -y -i "$input" $filter -pass 2 $video_params $audio_params "$outputTmp"
fi
duration=$(( SECONDS - start ))
# outputDimensions=$(mediainfo --Inform='Video;%Width%x%Height%' "$outputTmp")
# outputFinal="$input_without_extension.$preset.$audio_tag.$outputDimensions.mp4"
outputFinal="$input_without_extension.$preset.$audio_tag.mp4"
mv "$outputTmp" "$outputFinal"
echo ""
echo -e "$logo job's done"
echo -e "$logo encoded in $duration seconds"
echo -e "$logo final output is ${underline}${outputFinal}${reset}"

# ------------------------------------------------
# do some cleaning
# ------------------------------------------------
if [ -f "ffmpeg2pass-0.log" ]; then
  unlink "ffmpeg2pass-0.log"
fi
if [ -f "ffmpeg2pass-0.log.mbtree" ]; then
  unlink "ffmpeg2pass-0.log.mbtree"
fi
