#!/bin/bash

# ff-x264-crf-tests.sh
# testing many CRF qualities
# then extract and save one frame (png) to compare results
# 
# the input file must be named : in.mp4
# 
# https://codeberg.org/jrm-omg/fftools

array=(25 30 35 40)
for q in "${array[@]}"
do
    echo "-----------------------------------------------"
    echo -e "\e[1;32m[\e[1;37mtest\e[1;32m]\e[0m $q"
    echo "-----------------------------------------------"
    start=$SECONDS
    ffmpeg -ss 0:0:5 -to 0:0:10 -y -i "in.mp4" -vf "scale=-2:1080" -c:v libx264 -crf ${q} -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart "x264_CRF_${q}.mp4"
    ffmpeg -y -i "x264_CRF_${q}.mp4" -vf "select=eq(n\,34)" -vframes 1 "x264_screen_CRF_${q}.png"
    duration=$(( SECONDS - start ))
    mv "x264_CRF_${q}.mp4" "x264_CRF_${q}_(${duration}).mp4"
done