#!/bin/bash

# ff-x264-bitrate-tests.sh
# testing many different video bitrates
# then extract and save PNG frames to compare results
# 
# ff-x264-bitrate-tests.sh <input>
# 
# https://codeberg.org/jrm-omg/fftools

input=$1;
input_without_extension="${input%%.*}"

if [ -z "${1}" ]; then
  echo "need an input file ;)";
  exit;
fi

if [ ! -d "frames" ]; then
  mkdir frames;
fi

# extracting frames (from the input file)
ffmpeg -ss 0:0:1 -to 0:0:2 -i "$input" "frames/%04d-${input_without_extension}.00original.png"

for bv in {500,1000,1200,1400,1500,1600,1800,2000,2500,3000,3500,4000}; do
  mx=$(echo "$bv * 2" | bc)
  bf=$(echo "$mx * 1.5" | bc)
  video_params="-c:v libx264 -b:v ${bv}k -maxrate ${mx}k -bufsize ${bf}k -tune film -profile:v high -level 4.1 -me_method umh -me_range 24 -rc-lookahead 60 -subq 9 -refs 4 -b_strategy 2 -trellis 2 -psy-rd 1.00:0.15 -direct-pred 3 -keyint_min 23 -x264-params no-fast-pskip=1 -movflags faststart"
  audio_params="-c:a libfdk_aac -b:a 128k"
  # first pass
  ffmpeg -y -i "$input" $filter -pass 1 $video_params $audio_params -an -f mp4 /dev/null
  # second pass
  output_without_extension="$input_without_extension.${bv}k.x264"
  ffmpeg -y -i "$input" $filter -pass 2 $video_params $audio_params "$output_without_extension.mp4"
  # extracting frames
  ffmpeg -ss 0:0:1 -to 0:0:2 -i "$output_without_extension.mp4" "frames/%04d-${output_without_extension}.png"
done

# some cleanings
if [ -f "ffmpeg2pass-0.log" ]; then
  unlink "ffmpeg2pass-0.log"
fi
if [ -f "ffmpeg2pass-0.log.mbtree" ]; then
  unlink "ffmpeg2pass-0.log.mbtree"
fi