#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Modules
import os
import time
import sys
import subprocess
import math
try:
    import ffmpy
except ImportError as e:
    print('Error -> ', e)
    print('Please "pip install" all required packages first.')
    sys.exit()

# Checkpoints
if len(sys.argv) < 3:
    print('Error : please provide all required arguments\n')
    print('Usage : versus.py <input> <x264 CRF>,<x265 CRF>,<AV1 CRF> (<output height>)')
    print('Example 1 : versus.py vid.mpeg 33,35,55 480')
    print('Example 2 : versus.py vid.mpeg 33,35,55 720')
    sys.exit()
elif len(sys.argv) == 3:
    outputHeight = '1080'
else:
    outputHeight = sys.argv[3]
inputFn = sys.argv[1]
if os.path.exists(inputFn) == False:
    print('Error', 'Input file does not exists.')
    sys.exit()
inputFnNoExt = os.path.splitext(os.path.basename(inputFn))[0]
crfs = sys.argv[2].split(',')
if len(crfs) < 3:
    print('Error : three CRF values are required')
    sys.exit()

# Process
c = 0
for vLib in ('libx264', 'libx265', 'libsvtav1'):
    crf = crfs[c]
    outputFn = f'{inputFnNoExt}-{outputHeight}p-{vLib}-q{crf}.mp4'
    if os.path.isfile(outputFn):
        print(f'Output already exists, skipping ({outputFn})')
    else:
        print(f'Encoding {outputFn} ...')
        ff = ffmpy.FFmpeg(
            inputs={inputFn: '-y -hide_banner -loglevel quiet -stats'},
            outputs={outputFn: f'-vf "scale=-2:{outputHeight}" -c:v {vLib} -crf {crf} -colorspace bt709 -movflags faststart -an'}
        )
        time_start = time.time_ns()
        ff.run()
        time_diff = (time.time_ns() - time_start) / 1000000000
        # duration = str(round(time_diff, 3))
        duration = math.floor(time_diff)
        print(f'{outputFn} encoded in {duration} seconds.')
        outputFilesize = math.floor(os.path.getsize(outputFn)/1024)
        with open('versus.log', 'a') as log:
            log.write(f'{outputHeight}p;{outputFilesize}kb;{duration}s;{vLib};q{crf};{outputFn}\n')
    c += 1
print('---')
print('Versus is over')
print('Bye!')