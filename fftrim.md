# FFtrim

FFtrim helps you trimming your video without re-encoding it.

## Usage

```sh
fftrim.sh <input> <start> <end>
```

- input : your video input filename
- start : the timestamp where you wanna start your video from
- end : the timestamp where you wanna stop your video

Example :

```sh
fftrim.sh job.mp4 00:02:31 02:45:38
```
