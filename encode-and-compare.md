# Encode & Compare

A Python script that helps you comparing different video codecs and CRF settings.

 (see the explanation about CRF on the [FFmpeg wiki](https://trac.ffmpeg.org/wiki/Encode/H.264#crf)).

For now, testing :

```python
presets = {
    'x264': {
        'lib': 'libx264',
        'crf': range(20, 41)
    },
    'x265': {
        'lib': 'libx265',
        'crf': range(20, 41)
    },
    'AV1': {
        'lib': 'libsvtav1',
        'crf': range(40, 64)
    }
}
```

## Prerequisite

- [FFmpeg](https://ffmpeg.org/download.html)
- Python 3+
- Python's `ffmpy` module :

   ```sh
   pip install ffmpy
   ```

## Usage

Just run the script on a video file

**Usage**

```text
python3 encode-and-compare.py <input_file>
```

🔥 Caution : use a short video (like 10 seconds) so the script can quickly test all the presets. The longer the video is, the longer the benchmark will be!

**Demo**

```sh
python3 encode-and-compare.py sample.mp4
```

## What is CRF ?

CRF means « Constant Rate Factor ».

> Use this rate control mode if you want to keep the best quality and care less about the file size. This is the recommended rate control mode for most uses.
>
> This method allows the encoder to attempt to achieve a certain output quality for the whole file when output file size is of less importance. This provides maximum compression efficiency with a single pass. By adjusting the so-called quantizer for each frame, it gets the bitrate it needs to keep the requested quality level. The downside is that you can't tell it to get a specific filesize or not go over a specific size or bitrate, which means that this method is not recommended for encoding videos for streaming.
>
> — [FFmpeg wiki](https://trac.ffmpeg.org/wiki/Encode/H.264#crf)

## Max height output

Optionally, you can specify an output max height (like 480 or 720 or 1080 or whatever).

**Usage**

```text
python3 encode-and-compare.py <input_file> (<output height>)
```

**Demo**

```sh
python3 encode-and-compare.py sample.mp4 480
```
