#!/bin/bash

# av1.sh
# helping you encoding AV1 videos
# 
# https://codeberg.org/jrm-omg/fftools

# install, so you can run it from everywhere
# sudo cp av1.sh /usr/local/bin/

if [ -z $1 ]; then
  echo ""
  echo "Usage: av1.sh <input> [<options>]"
  exit
fi

# input modifier (if using find)
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# input pathinfo
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}")
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

output_basename=${input_filename}.1080p.av1.opus.mp4
echo Encoding ${output_basename}...
ffmpeg -i "${input}" -hide_banner -loglevel quiet -stats -vf "scale=-2:1080" -c:v libsvtav1 -crf 50 -colorspace bt709 -movflags faststart -c:a libopus -b:a 128K "${output_basename}"
echo "Job's done, bye!"