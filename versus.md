# Versus

Batch testing CRF for x264, x265 and AV1.

Also appending results to a `versus.log` file (file size, encoding duration, etc.)

## Prerequisite

- [FFmpeg](https://ffmpeg.org/download.html)
- Python 3+
- Python's `ffmpy` module :

   ```sh
   pip install ffmpy
   ```

## Usage

Just run the script on a video file, and specify three [CRF](https://trac.ffmpeg.org/wiki/Encode/H.264#crf) values (comma separated) :

1. First value for x264
2. Second one for x265
3. Third one for AV1

**Usage**

```text
python3 versus.py <input_file> <x264 CRF>,<x265 CRF>,<AV1 CRF>
```

**Demo**

```sh
python3 versus.py sample.mp4 33,35,55
```

## Max height output

Optionally, you can specify an output max height (like 480 or 720 or 1080 or whatever).

**Usage**

```text
python3 versus.py <input_file> <x264 CRF>,<x265 CRF>,<AV1 CRF> (<output height>)
```

**Demo**

```sh
python3 versus.py sample.mp4 33,35,55 480
```

## Sample

If you need a sample you can freely [download this 30 secs 4K video](https://fmr.tf/s/waves-2160p.mp4), just could just use wget for that :

```sh
wget https://fmr.tf/s/waves-2160p.mp4 -O sample.mp4
```
