#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Modules
import os
import time
import sys
import subprocess
import json
try:
    import ffmpy
except ImportError as e:
    print('Error -> ', e)
    print('Please "pip install" all required packages first.')
    sys.exit()

# Functions
def mkdirIfNoExists(dirname):
    if os.path.isdir(dirname) == False:
        os.mkdir(dirname)

# Define all codecs and params to test
presets = {
    'x264': {
        'lib': 'libx264',
        'crf': range(22, 39, 2)
    },
    'x265': {
        'lib': 'libx265',
        'crf': range(22, 39, 2)
    },
    'AV1': {
        'lib': 'libsvtav1',
        'crf': range(40, 64, 2)
    }
}

# Input checkpoint
if len(sys.argv) == 1:
    print('Error : Please provide an input filename.', '\n\nScript usage :', '\nencode-and-compare.py <input> (<output height (integer)>)')
    sys.exit()
elif len(sys.argv) == 2:
    outputHeight = '1080'
else:
    outputHeight = sys.argv[2]
inputFn = sys.argv[1]
if os.path.exists(inputFn) == False:
    print('Error', 'Input file does not exists.')
    sys.exit()

# Input filename without extension
inputFnNoExt = os.path.splitext(os.path.basename(inputFn))[0]

# Input duration
ff = ffmpy.FFprobe(
    inputs={inputFn: '-hide_banner -loglevel quiet -show_streams -select_streams v:0 -of json'}
)
res = ff.run(stdout=subprocess.PIPE)
meta = json.loads(res[0].decode('utf-8'))
lenTotal = float(meta['streams'][0]['duration'])
lenPart = lenTotal / 8

# Batch testing, go go go!
for vFormat in presets.keys():
    mkdirIfNoExists(vFormat)
    for crf in presets[vFormat]['crf']:
        lib = presets[vFormat]['lib']
        outputFn = f'{vFormat}/{inputFnNoExt}-{outputHeight}p-{vFormat}-q{crf}.mp4'
        print('╔══════════════════════════════════════════╗')
        print(f'║ ▓▒░       {vFormat.rjust(4)} {outputHeight.rjust(4)}p @ crf {crf}        ░▒▓ ║')
        print('╚══════════════════════════════════════════╝')
        ff = ffmpy.FFmpeg(
            inputs={inputFn: '-y -hide_banner -loglevel quiet -stats'},
            outputs={outputFn: f'-vf "scale=-2:{outputHeight}" -c:v {lib} -crf {crf} -colorspace bt709 -movflags faststart -an'}
        )
        time_start = time.time_ns()
        ff.run()
        time_diff = (time.time_ns() - time_start) / 1000000000
        print(f'{outputFn} encoded in', round(time_diff, 3), 'seconds.')
        print('Extracting a couple of frames...', end='', flush=True)
        for i in range(8):
            mkdirIfNoExists(f'{vFormat}/screen_{i}')
            screenFn = f'{vFormat}/screen_{i}/{inputFnNoExt}-{outputHeight}p-{vFormat}-screen_{i}-q{crf}.png'
            sFirst = round(lenPart * i, 2)
            sLast  = round(lenPart * i + 0.1, 2)
            ff = ffmpy.FFmpeg(
                inputs={outputFn: f'-ss {sFirst} -to {sLast} -y -hide_banner -loglevel quiet'},
                outputs={screenFn: '-update true'}
            )
            ff.run()
        print('Done')
        # Rename encoded video : add encoding duration
        duration = str(round(time_diff, 2))
        outputFn2 = f'{vFormat}/{inputFnNoExt}-{outputHeight}p-{vFormat}-q{crf}_({duration}).mp4'
        os.rename(outputFn, outputFn2)
print('All targeted presets have been tested.\n')
print('Now compare the different screenshots...\nHope you\'ll find the best preset that suites your needs.\n')
print('Bye!')