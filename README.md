# My FFmpeg scripts & tools

This repo contains my little scripts and helpers I've made regarding the so, so great [FFmpeg](https://en.wikipedia.org/wiki/FFmpeg) 💖

## Scripts

- 🆕 **ffaudio.py** encode audio files to flac, opus, ogg or mp3.
- **ff-crf.tests.py** batch testing CRF any [FFmpeg enabled encoders](https://ffmpeg.org/ffmpeg-codecs.html#Encoders).
- **encode-and-compare.py** batch testing encodings using many different x264, x265 and AV1 presets, and extract specific frames for each test. See its [documentation](encode-and-compare.md) here.
- **versus.py** batch testing CRF for x264, x265 and AV1. See its [documentation](versus.md) here.

### Linux install

Using Linux, you could download & install any script following this example :

```sh
sudo wget https://codeberg.org/jrm-omg/fftools/raw/branch/main/ffaudio.py -O /usr/local/bin/ffaudio.py; \
sudo chmod +x /usr/local/bin/ffaudio.py;
```

Simply replace `ffaudio.py` by the script you wanna install.

### Linux shell scripts

- **av1.sh** simple stupid AV1/Opus encoder (best video codec out there!)
- **fftrim.sh** trim your video without re-encoding it. See its [documentation](fftrim.md) here.
- **ffmono.sh** from a video, map one audio channel to both stereo channel.
- [ffmpeg-cpu-benchmark](https://codeberg.org/jrm-omg/ffmpeg-cpu-benchmark) test your CPU speed (and energy consumption) and have fun comparing your result with others! (hosted on a separated repo)
- **x264-Expecto-Patronum.sh** make x264 video using the best size/quality ratio ever (YIFY style, if you know what I mean). See the [documentation](./x264-Expecto-Patronum.md) here.
- **ff-av1-crf-tests.sh** batch testing AV1 CRFs.
- **ff-x264-crf-tests.sh** batch testing x264 CRFs.
- **ff-x265-crf-tests.sh** batch testing x265 CRFs.
- **ff-x264-bitrate-tests.sh** batch testing x264 bitrates.

### Personal video CRF choices

> This is so subjective, warning, woop woop.

Lowest file size while keeping a quality, even when the cam is moving a lot.

| Codec | CRF MQ | CRF HQ |
| ----: | :----: | :----: |
|  x264 |   30   |   22   |
|  x265 |   30   |   26   |
|   AV1 |   44   |   36   |

Sample command lines :

> You can prevent re-encoding the audio using `-c:a copy`

**x264**

```sh
ffmpeg -y -i <INPUT> -vf "scale=-2:720" -c:v libx264 -crf 30 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart <OUTPUT>
```

**x265**

```sh
ffmpeg -y -i <INPUT> -vf "scale=-2:720" -c:v libx265 -crf 30 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart <OUTPUT>
```

**AV1**

```sh
ffmpeg -y -i <INPUT> -vf "scale=-2:720" -c:v libsvtav1 -crf 44 -colorspace bt709 -c:a libfdk_aac -b:a 128k -movflags faststart <OUTPUT>
```

## Sample

If you need a video sample, download a [8k video from Pexels](https://www.pexels.com/search/videos/8k%20resolution/), or [download this 30 secs 4K video](https://fmr.tf/s/waves-2160p.mp4), for instance using a quick `wget https://fmr.tf/s/waves-2160p.mp4 -O sample.mp4`

## Links

- https://trac.ffmpeg.org/wiki/Encode/AV1
- https://trac.ffmpeg.org/wiki/Encode/H.264
- https://trac.ffmpeg.org/wiki/Encode/H.265
- https://wiki.hydrogenaud.io/index.php?title=Lossless_comparison#Comparison_Table
- https://wiki.xiph.org/index.php?title=Opus_Recommended_Settings#Recommended_Bitrates
- https://wiki.hydrogenaud.io/index.php?title=Recommended_Ogg_Vorbis
- https://wiki.hydrogenaud.io/index.php?title=LAME

## Credits

Made FTW by [Jérémie CANAVESIO](https://humanize.me/now.html) and licensed under [AGPL](LICENSE).
