#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Built-in modules
import sys
import os
import time
import math

# External modules
try:
    import ffmpy
except ImportError as e:
    print('[ffaudio] ERROR -> ', e)
    print('[ffaudio] Please "pip install" all required packages first.')
    sys.exit()


# Required arguments
if len(sys.argv) <= 2:
    # Splash
    print('╔═══════════════════════════════════════════════════╗')
    print('║ ffaudio.py - https://codeberg.org/jrm-omg/fftools ║')
    print('╚═══════════════════════════════════════════════════╝')
    print('')
    print('Please provide all required arguments')
    print('Usage : ffaudio.py <input> <preset>')
    print('')
    print('Presets, based on quality produced from high to low are :')
    print('flac, opus, ogg, mp3, mp3-192')
    print('')
    print('Batch process example :')
    print(f'find -iname "*.flac" -exec ffaudio.py "{{}}" mp3 \\;')
    sys.exit()

# Define presets
presets = {
    'flac': {
        'library'   : 'flac',
        'extension' : 'flac',
        'command'   : '-compression_level 12' # max compression level
    },
    'opus': {
        'library'   : 'libopus',
        'extension' : 'opus',
        'command'   : '-b:a 128k' # VBR by default
                                  # Opus 128kbps VBR is a "a pretty much transparent lossy compression"
                                  # https://wiki.xiph.org/index.php?title=Opus_Recommended_Settings#Recommended_Bitrates
    },
    'ogg': {
        'library'   : 'libvorbis',
        'extension' : 'ogg',
        'command'   : '-q:a 6' # the higher the better
    },
    'mp3': {
        'library'   : 'libmp3lame',
        'extension' : 'mp3',
        'command'   : '-q:a 0' # the lower the better
    },
    'mp3-192': {
        'library'   : 'libmp3lame',
        'extension' : 'mp3',
        'command'   : '-compression_level 0 -b:a 192k'
    },
}
p = sys.argv[2]

# Preset check
if p not in presets:
    print(f'[ffaudio] ERROR : Preset "{p}" does not exists.')
    sys.exit()

# Input check
input_fn = sys.argv[1]
if os.path.exists(input_fn) == False:
    sys.exit(f'[ffaudio] ERROR : Input file "{input_fn}" does not exists.')
input_noext = os.path.splitext(os.path.basename(input_fn))[0]

# Process
output_fn = f'{input_noext}.{presets[p]['extension']}'
print(f'[ffaudio] Encoding "{output_fn}" ...')
ff = ffmpy.FFmpeg(
    inputs={input_fn: '-y -hide_banner -loglevel quiet -stats'},
    outputs={output_fn: f'-c:a {presets[p]['library']} {presets[p]['command']}'}
)
time_start = time.time_ns()
ff.run()
time_diff = math.floor((time.time_ns() - time_start) / 1000000000)

# Job's done
print(f'[ffaudio] Job\'s done! "{output_fn}" encoded in {time_diff} s.')